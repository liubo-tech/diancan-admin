package com.diancan.admin.service.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Product implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名称
     */
    private String productTitle;

    /**
     * 商品状态（0：上架；1：下架）
     */
    private Integer productStatus;

    /**
     * 商品价格
     */
    private BigDecimal productPrice;

    /**
     * 商品单位id
     */
    private Integer productUnitId;

    /**
     * 商品描述
     */
    private String productDescription;

    /**
     * 商户的openid
     */
    private String openid;

    /**
     * 0：正常；1：删除
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    private Integer updateUser;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private MerchantInfo store;

    @TableField(exist = false)
    private List<ProductImage> imageList;

    @TableField(exist = false)
    private String productUnitName;
}
