package com.diancan.admin.service.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MerchantInfo implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * openid
     */
    private String openid;

    /**
     * 小店名称
     */
    private String storeName;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 身份证号
     */
    private String cardNo;

    /**
     * 楼门号
     */
    private String houseNo;

    /**
     * 配送范围（单位：公里）
     */
    private Integer scope;

    /**
     * 身份证（正面）
     */
    private String cardFront;

    /**
     * 身份证（反面）
     */
    private String cardBack;

    /**
     * 手持身份证
     */
    private String cardHand;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 位置名称
     */
    private String locationName;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 精度
     */
    private BigDecimal longitude;

    /**
     * 状态（0：驳回；1：待审核；2：通过）
     */
    private Integer status;

    /**
     * 驳回理由
     */
    private String rejectReason;

    /**
     * 删除标识位（0：正常；1：删除）
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     *  更新人（后台用户）
     */
    private Integer updateUser;

}
