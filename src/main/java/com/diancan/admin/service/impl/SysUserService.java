package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.SysPermission;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.mapper.SysPermissionMapper;
import com.diancan.admin.service.mapper.SysUserMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserService implements UserDetailsService {

    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private SysPermissionMapper permissionMapper;

    public SysUser selectByUsername(String username) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);

        return userMapper.selectOne(queryWrapper);
    }


    public SysUser selectById(Integer userId) {
        SysUser sysUser = userMapper.selectById(userId);
        if (sysUser == null) return sysUser;

        List<SysPermission> permissions = permissionMapper.selectByUserId(userId);
        sysUser.setPermissions(permissions);
        return sysUser;
    }

    public IPage<SysUser> selectByCondition(Page<SysUser> page, SysUser condition) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight(StringUtils.isNotBlank(condition.getUsername()),"username",condition.getUsername());

        return userMapper.selectPage(page,queryWrapper);
    }

    public int updateUser(SysUser sysUser) {
        return userMapper.updateById(sysUser);
    }

    public int insertSysUser(SysUser sysUser) {
        return userMapper.insert(sysUser);
    }

    public int deleteById(Integer userId) {
        return userMapper.deleteById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);

        SysUser sysUser = userMapper.selectOne(queryWrapper);
        if (sysUser == null) throw new UsernameNotFoundException("用户名或密码错误");

        if (isAdmin(username)) {
            List<SysPermission> permissions = permissionMapper.selectList(new QueryWrapper<>());
            sysUser.setPermissions(permissions);
        } else {
            List<SysPermission> permissions = permissionMapper.selectByUserId(sysUser.getId());
            sysUser.setPermissions(permissions);
        }
        return sysUser;
    }

    private boolean isAdmin(String username) {
        return "admin".equals(username);
    }
}
