package com.diancan.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.SysRole;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.SysRoleService;
import com.diancan.admin.service.impl.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Set;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysRoleService roleService;

    @PreAuthorize("hasAuthority('system:user')")
    @RequestMapping("list")
    public RespResult selectByCondition(Page<SysUser> page, SysUser condition) {
        RespResult respResult = new RespResult();
        respResult.setData(userService.selectByCondition(page,condition));

        return respResult;
    }

    @PreAuthorize("hasAuthority('system:user')")
    @RequestMapping("detail")
    public RespResult detail(Integer userId) {
        RespResult respResult = new RespResult();
        respResult.setData(userService.selectById(userId));
        return respResult;
    }

    @PreAuthorize("hasAuthority('system:user')")
    @RequestMapping("delUser")
    public RespResult delUser(Integer userId) {
        RespResult respResult = new RespResult();
        userService.deleteById(userId);
        return respResult;
    }

    @PreAuthorize("hasAuthority('system:user')")
    @RequestMapping("update")
    public RespResult update(SysUser sysUser, @AuthenticationPrincipal SysUser currentUser) {
        RespResult respResult = new RespResult();

        //新增
        if (sysUser.getId() == null) {
            LocalDateTime now = LocalDateTime.now();
            sysUser.setSalt(sysUser.getPassword());
            sysUser.setCreateTime(now);
            sysUser.setCreateUser(currentUser.getId());
            sysUser.setUpdateTime(now);
            sysUser.setUpdateUser(currentUser.getId());
            userService.insertSysUser(sysUser);

        } else {
            SysUser user = userService.selectById(sysUser.getId());
            if (user == null) {
                respResult.setCode(1);
                respResult.setMessage("用户已被删除");
                return respResult;
            }

            sysUser.setUpdateTime(LocalDateTime.now());
            sysUser.setUpdateUser(currentUser.getId());

            userService.updateUser(sysUser);
        }

        return respResult;
    }

    @PreAuthorize("hasAuthority('system:user')")
    @RequestMapping("searchUserRole")
    public RespResult searchUserRole(Page<SysRole> page, SysRole sysRole, Integer userId) {
        IPage<SysRole> iPage = roleService.selectByCondition(page, sysRole);

        Set<Integer> roleIds = roleService.getRolesByUserId(userId);

        for (SysRole record : iPage.getRecords()) {
            record.setChecked(roleIds.contains(record.getId()));
        }
        return RespResult.success(iPage);
    }
}
