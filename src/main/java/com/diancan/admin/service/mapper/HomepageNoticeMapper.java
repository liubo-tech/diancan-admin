package com.diancan.admin.service.mapper;

import com.diancan.admin.service.entity.HomepageNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-07-17
 */
public interface HomepageNoticeMapper extends BaseMapper<HomepageNotice> {

}
