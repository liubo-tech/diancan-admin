/*
Navicat MySQL Data Transfer

Source Server         : 本地数据库
Source Server Version : 80014
Source Host           : localhost:3306
Source Database       : school

Target Server Type    : MYSQL
Target Server Version : 80014
File Encoding         : 65001

Date: 2020-07-13 13:42:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `permission_name` varchar(255) NOT NULL COMMENT '权限名称',
  `permission_flag` varchar(255) NOT NULL COMMENT '权限标识',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '上级权限id（如没有，则为0）',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型（1：菜单；2：按钮；）',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除标志（0：正常；1：删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_permission_name` (`permission_name`),
  KEY `sys_permission_flag` (`permission_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('2', '系统管理', 'system', '0', '1', '0', '2020-06-28 15:09:19', '10001', '2020-06-28 15:09:19', '10001');
INSERT INTO `sys_permission` VALUES ('3', '用户管理', 'system:user', '2', '1', '0', '2020-06-28 15:15:38', '10001', '2020-06-28 15:15:38', '10001');
INSERT INTO `sys_permission` VALUES ('4', '角色管理', 'system:role', '2', '1', '0', '2020-06-28 15:23:18', '10001', '2020-06-28 15:23:18', '10001');
INSERT INTO `sys_permission` VALUES ('5', '权限管理', 'system:permission', '2', '1', '0', '2020-06-28 15:23:42', '10001', '2020-06-28 16:51:38', '10001');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_name` varchar(255) NOT NULL COMMENT '角色名称',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0：正常；1：禁用',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除字段（0：未删除；1：删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('2', '总经理', '0', '0', '2020-06-28 15:00:56', '10001', '2020-06-28 15:00:56', '10001');
INSERT INTO `sys_role` VALUES ('3', '财务经理', '0', '0', '2020-06-28 15:01:32', '10001', '2020-06-28 15:01:32', '10001');
INSERT INTO `sys_role` VALUES ('4', '系统管理员', '0', '0', '2020-07-01 14:01:09', '10001', '2020-07-01 14:01:09', '10001');

-- ----------------------------
-- Table structure for sys_role_permission_rel
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission_rel`;
CREATE TABLE `sys_role_permission_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `permission_id` int(11) NOT NULL COMMENT '权限id',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除（0：正常；1：删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role_permission_rel
-- ----------------------------
INSERT INTO `sys_role_permission_rel` VALUES ('1', '1', '1', '0', '2020-06-23 13:47:00', '1', '2020-06-23 13:47:02', '1');
INSERT INTO `sys_role_permission_rel` VALUES ('15', '2', '3', '0', '2020-07-01 11:19:49', '10003', '2020-07-01 11:19:49', '10003');
INSERT INTO `sys_role_permission_rel` VALUES ('16', '2', '4', '0', '2020-07-01 11:19:49', '10003', '2020-07-01 11:19:49', '10003');
INSERT INTO `sys_role_permission_rel` VALUES ('17', '2', '5', '0', '2020-07-01 11:19:49', '10003', '2020-07-01 11:19:49', '10003');
INSERT INTO `sys_role_permission_rel` VALUES ('18', '4', '2', '0', '2020-07-01 14:01:17', '10001', '2020-07-01 14:01:17', '10001');
INSERT INTO `sys_role_permission_rel` VALUES ('19', '4', '3', '0', '2020-07-01 14:01:17', '10001', '2020-07-01 14:01:17', '10001');
INSERT INTO `sys_role_permission_rel` VALUES ('20', '4', '4', '0', '2020-07-01 14:01:17', '10001', '2020-07-01 14:01:17', '10001');
INSERT INTO `sys_role_permission_rel` VALUES ('21', '4', '5', '0', '2020-07-01 14:01:17', '10001', '2020-07-01 14:01:17', '10001');
INSERT INTO `sys_role_permission_rel` VALUES ('22', '4', '7', '0', '2020-07-01 14:01:17', '10001', '2020-07-01 14:01:17', '10001');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `salt` varchar(32) NOT NULL COMMENT '盐',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态（0：正常；1：锁定）',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除字段（0：未删除；1：删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sys_user_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10005 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('10001', 'admin', '123456', '1', '0', '0', '2019-12-26 16:42:52', '1', '2020-06-24 16:40:53', '10001');
INSERT INTO `sys_user` VALUES ('10003', 'liuzhuo', '1111qqqq', '123456', '0', '0', '2020-06-24 17:12:25', '10001', '2020-06-24 17:12:59', '10001');
INSERT INTO `sys_user` VALUES ('10004', 'liubo', '123456', '123456', '0', '0', '2020-07-01 13:47:43', '10001', '2020-07-01 13:47:43', '10001');

-- ----------------------------
-- Table structure for sys_user_role_rel
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_rel`;
CREATE TABLE `sys_user_role_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '删除字段（0：正常；1：删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_user_role_rel
-- ----------------------------
INSERT INTO `sys_user_role_rel` VALUES ('1', '10001', '1', '0', '2020-06-23 13:46:12', '1', '2020-06-23 13:46:15', '1');
INSERT INTO `sys_user_role_rel` VALUES ('5', '10003', '2', '0', '2020-07-01 09:03:10', '10001', '2020-07-01 09:03:10', '10001');
INSERT INTO `sys_user_role_rel` VALUES ('9', '10004', '4', '0', '2020-07-01 14:01:20', '10001', '2020-07-01 14:01:20', '10001');

-- ----------------------------
-- Table structure for s_class
-- ----------------------------
DROP TABLE IF EXISTS `s_class`;
CREATE TABLE `s_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '班级名称(不要写年级，写学籍)',
  `class_year` year(4) NOT NULL COMMENT '入籍年份',
  `school_id` int(11) NOT NULL COMMENT '学校id',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型：1',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0（正常）；1（删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='班级表';

-- ----------------------------
-- Records of s_class
-- ----------------------------
INSERT INTO `s_class` VALUES ('1', '1', '2020', '1', '1', '1', '2020-02-27 00:00:00', '1', '2020-02-27 00:00:00', '1');

-- ----------------------------
-- Table structure for s_class_course_rel
-- ----------------------------
DROP TABLE IF EXISTS `s_class_course_rel`;
CREATE TABLE `s_class_course_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL COMMENT '课程id',
  `course_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL COMMENT '老师id',
  `school_id` int(11) NOT NULL COMMENT '学校id',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0:正常；1：删除',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='班级——课程关系';

-- ----------------------------
-- Records of s_class_course_rel
-- ----------------------------

-- ----------------------------
-- Table structure for s_course
-- ----------------------------
DROP TABLE IF EXISTS `s_course`;
CREATE TABLE `s_course` (
  `id` int(11) NOT NULL COMMENT '课程id',
  `course_name` varchar(255) NOT NULL COMMENT '课程名称',
  `school_id` int(11) NOT NULL COMMENT '学校id',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类型：1',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0（正常）；1（删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='课程';

-- ----------------------------
-- Records of s_course
-- ----------------------------

-- ----------------------------
-- Table structure for s_school
-- ----------------------------
DROP TABLE IF EXISTS `s_school`;
CREATE TABLE `s_school` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `school_name` varchar(255) NOT NULL COMMENT '学校名称',
  `school_type` int(2) NOT NULL DEFAULT '1' COMMENT '类型：1（小学）；',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0（正常）；1（停用）',
  `province_id` bigint(12) DEFAULT NULL,
  `province_name` varchar(255) DEFAULT NULL,
  `city_id` bigint(12) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `region_id` bigint(12) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='学校表';

-- ----------------------------
-- Records of s_school
-- ----------------------------

-- ----------------------------
-- Table structure for s_student
-- ----------------------------
DROP TABLE IF EXISTS `s_student`;
CREATE TABLE `s_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(255) NOT NULL COMMENT '学生姓名',
  `school_id` int(11) NOT NULL COMMENT '学校id',
  `sex` int(1) NOT NULL DEFAULT '0' COMMENT '性别：0（未填）；1：男；2：女',
  `birthday` date DEFAULT NULL COMMENT '出生年月',
  `province_id` bigint(12) DEFAULT NULL,
  `province_name` varchar(255) DEFAULT NULL,
  `city_id` bigint(12) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `region_id` bigint(12) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `linkman` varchar(255) DEFAULT NULL COMMENT '联系人',
  `link_mobile` varchar(11) DEFAULT NULL COMMENT '联系人手机号',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0（正常）；1（删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='学生表';

-- ----------------------------
-- Records of s_student
-- ----------------------------

-- ----------------------------
-- Table structure for s_student_class_rel
-- ----------------------------
DROP TABLE IF EXISTS `s_student_class_rel`;
CREATE TABLE `s_student_class_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0：正常；1：删除',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of s_student_class_rel
-- ----------------------------

-- ----------------------------
-- Table structure for s_teacher
-- ----------------------------
DROP TABLE IF EXISTS `s_teacher`;
CREATE TABLE `s_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_name` varchar(255) NOT NULL,
  `school_id` int(11) NOT NULL COMMENT '学校id',
  `sex` int(1) NOT NULL DEFAULT '0' COMMENT '性别：0（未填）；1（男）；2（女）',
  `birthday` date DEFAULT NULL,
  `province_id` bigint(12) DEFAULT NULL,
  `province_name` varchar(255) DEFAULT NULL,
  `city_id` bigint(12) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `region_id` bigint(12) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0（正常）；1（删除）',
  `create_time` datetime NOT NULL,
  `create_user` int(11) NOT NULL DEFAULT '0',
  `update_time` datetime NOT NULL,
  `update_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='老师表';

-- ----------------------------
-- Records of s_teacher
-- ----------------------------

-- ----------------------------
-- Table structure for s_term
-- ----------------------------
DROP TABLE IF EXISTS `s_term`;
CREATE TABLE `s_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `term_year` year(4) NOT NULL COMMENT '学期年份',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1：秋季；2：春季',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of s_term
-- ----------------------------
INSERT INTO `s_term` VALUES ('15', '2020', '1');
INSERT INTO `s_term` VALUES ('19', '2020', '1');
