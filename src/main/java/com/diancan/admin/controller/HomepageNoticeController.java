package com.diancan.admin.controller;

import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.HomepageNotice;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.HomepageNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("homepage/notice")
public class HomepageNoticeController {
    @Autowired
    private HomepageNoticeService noticeService;

    @PreAuthorize("hasAuthority('homepage:notice')")
    @RequestMapping("load")
    public RespResult loadNotice() {
        HomepageNotice notice = noticeService.loadHomepageNotice();
        return RespResult.success(notice);
    }

    @PreAuthorize("hasAuthority('homepage:notice')")
    @RequestMapping("changeStatus")
    public RespResult changeStatus(@RequestParam(defaultValue = "false") Boolean status,
                                   @AuthenticationPrincipal SysUser sysUser) {
        noticeService.changeStatus(status,sysUser.getId());
        return RespResult.success();
    }

    @PreAuthorize("hasAuthority('homepage:notice')")
    @RequestMapping("updateNotice")
    public RespResult updateNotice(HomepageNotice notice,
                                   @AuthenticationPrincipal SysUser sysUser) {

        noticeService.updateNotice(notice,sysUser.getId());
        return RespResult.success();
    }
}
