package com.diancan.admin.config;

import com.diancan.admin.service.impl.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.ForwardLogoutSuccessHandler;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private SysUserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .formLogin(form -> {
                form.usernameParameter("username")
                        .passwordParameter("password")
                        .successForwardUrl("/system/loginSuccess")
                        .failureForwardUrl("/system/loginFailure")
                        .loginProcessingUrl("/system/login")
                        .loginPage("/system/needLogin")
                ;
            })
            .logout(logout -> {
                logout.logoutUrl("/system/logout")
                .logoutSuccessHandler(new ForwardLogoutSuccessHandler("/system/logoutSuccess"));
                        ;
            })
            .csrf(csrf->{
                csrf.disable();
            })
            .cors(withDefaults())
            .userDetailsService(userService)
            .authorizeRequests(authorize -> {
                List<String> urls = new ArrayList<>();
                urls.add("/system/login");
                urls.add("/system/logoutSuccess");
                urls.add("/system/loginFailure");
                urls.add("/system/needLogin");

                authorize.antMatchers(urls.toArray(new String[0])).permitAll()
                .anyRequest().authenticated();
            })
        ;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
