package com.diancan.admin.service.mapper;

import com.diancan.admin.service.entity.SysRolePermissionRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-06-22
 */
public interface SysRolePermissionRelMapper extends BaseMapper<SysRolePermissionRel> {

}
