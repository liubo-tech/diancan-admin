package com.diancan.admin.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "qiniu")
@Setter@Getter
public class QiniuConfig {

    private String imgPrefix;

    private String accessKey;
    private String secretKey;
    private String bucket;

}
