package com.diancan.admin.service.mapper;

import com.diancan.admin.service.entity.HomepageNav;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-07-21
 */
public interface HomepageNavMapper extends BaseMapper<HomepageNav> {

}
