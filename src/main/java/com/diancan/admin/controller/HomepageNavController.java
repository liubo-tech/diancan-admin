package com.diancan.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.common.RespResult;
import com.diancan.admin.config.QiniuConfig;
import com.diancan.admin.service.entity.HomepageNav;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.HomepageNavService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("homepage/nav")
public class HomepageNavController extends BaseController {
    @Autowired
    private HomepageNavService navService;

    @PreAuthorize("hasAuthority('homepage:nav')")
    @RequestMapping("list")
    public RespResult selectByCondition(Page<HomepageNav> page, HomepageNav condition) {
        RespResult respResult = new RespResult();
        IPage<HomepageNav> navIPage = navService.selectByCondition(page, condition);
        respResult.setData(navIPage);

        return respResult;
    }

    @PreAuthorize("hasAuthority('homepage:nav')")
    @RequestMapping("update")
    public RespResult update(HomepageNav nav, @AuthenticationPrincipal SysUser sysUser) {
        LocalDateTime now = LocalDateTime.now();
        //新增
        if (nav.getId() == null) {
            nav.setCreateUser(sysUser.getId());
            nav.setUpdateUser(sysUser.getId());
            nav.setCreateTime(now);
            nav.setUpdateTime(now);
            navService.addNav(nav);
        } else {//修改
            nav.setUpdateUser(sysUser.getId());
            nav.setUpdateTime(now);
            navService.updateNav(nav);
        }


        return RespResult.success();
    }

    @PreAuthorize("hasAuthority('homepage:nav')")
    @RequestMapping("delete")
    public RespResult delete(Integer navId) {
        navService.deleteByNavId(navId);
        return RespResult.success();
    }
    @PreAuthorize("hasAuthority('homepage:nav')")
    @RequestMapping("changeStatus")
    public RespResult changeStatus(Integer navId,@AuthenticationPrincipal SysUser sysUser) {
        navService.changeStatus(navId,sysUser.getId());
        return RespResult.success();
    }
    @PreAuthorize("hasAuthority('homepage:nav')")
    @RequestMapping("detail")
    public RespResult detail(Integer navId) {
        HomepageNav carousel = navService.selectById(navId);
        return RespResult.success(carousel);
    }

}
