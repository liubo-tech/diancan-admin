package com.diancan.admin.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-07-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class HomepageNotice implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 通知内容
     */
    private String noticeText;

    /**
     * 跳转地址
     */
    private String noticeUrl;

    /**
     * 0：开；1：关
     */
    private Boolean status;

    /**
     * 0：正常；1：删除
     */
    @JsonIgnore
    private Integer deleted;
    @JsonIgnore
    private Integer createUser;
    @JsonIgnore
    private LocalDateTime createTime;
    @JsonIgnore
    private Integer updateUser;
    @JsonIgnore
    private LocalDateTime updateTime;


}
