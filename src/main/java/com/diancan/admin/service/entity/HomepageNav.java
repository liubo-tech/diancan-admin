package com.diancan.admin.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class HomepageNav implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 导航名称
     */
    private String navName;

    /**
     * 导航图片
     */
    private String navImg;

    /**
     * 导航跳转地址
     */
    private String navTarget;

    /**
     * 状态（0：启用；1：停用）
     */
    private Integer status;

    /**
     * 删除（0：正常；1：删除）
     */
    @JsonIgnore
    private Integer deleted;
    @JsonIgnore
    private LocalDateTime createTime;
    @JsonIgnore
    private Integer createUser;
    @JsonIgnore
    private LocalDateTime updateTime;
    @JsonIgnore
    private Integer updateUser;


}
