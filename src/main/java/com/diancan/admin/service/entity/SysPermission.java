package com.diancan.admin.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-06-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonIgnoreProperties(value = "children",allowGetters = true)
public class SysPermission implements Serializable, GrantedAuthority {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名称
     */
    private String permissionName;

    /**
     * 权限标识
     */
    private String permissionFlag;

    /**
     * 上级权限id（如没有，则为0）
     */
    private Integer parentId;

    /**
     * 类型（1：菜单；2：按钮；）
     */
    private Integer type;

    /**
     * 删除标志（0：正常；1：删除）
     */
    @JsonIgnore
    private Integer deleted;
    @JsonIgnore
    private LocalDateTime createTime;
    @JsonIgnore
    private Integer createUser;
    @JsonIgnore
    private LocalDateTime updateTime;
    @JsonIgnore
    private Integer updateUser;

    @TableField(exist = false)
    private List<SysPermission> children;

    @TableField(exist = false)
    private String parentName;


    @Override
    public String getAuthority() {
        return permissionFlag;
    }
}
