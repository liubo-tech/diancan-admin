package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.MerchantInfo;
import com.diancan.admin.service.mapper.MerchantInfoMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static com.diancan.admin.common.Constant.MERCHANT_STATUS_PASS;
import static com.diancan.admin.common.Constant.MERCHANT_STATUS_REJECT;

@Service
public class MerchantService {
    @Autowired
    private MerchantInfoMapper merchantInfoMapper;


    public IPage<MerchantInfo> selectByCondition(Page<MerchantInfo> page, MerchantInfo condition) {
        QueryWrapper<MerchantInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isNotBlank(condition.getStoreName()),"store_name",condition.getStoreName());
        queryWrapper.eq(condition.getStatus()!=null,"status",condition.getStatus());
        queryWrapper.eq(StringUtils.isNotBlank(condition.getMobile()),"mobile",condition.getMobile());
        queryWrapper.eq(StringUtils.isNotBlank(condition.getCardNo()),"card_no",condition.getCardNo());

        return merchantInfoMapper.selectPage(page,queryWrapper);
    }


    public int pass(Integer merchantId, Integer currentUserId) {
        MerchantInfo merchantInfo = new MerchantInfo();
        merchantInfo.setId(merchantId);
        merchantInfo.setStatus(MERCHANT_STATUS_PASS);
        merchantInfo.setUpdateTime(LocalDateTime.now());
        merchantInfo.setUpdateUser(currentUserId);

        return merchantInfoMapper.updateById(merchantInfo);
    }

    public int reject(Integer merchantId, String rejectMsg, Integer currentUserId) {
        MerchantInfo merchantInfo = new MerchantInfo();
        merchantInfo.setId(merchantId);
        merchantInfo.setStatus(MERCHANT_STATUS_REJECT);
        merchantInfo.setRejectReason(rejectMsg);
        merchantInfo.setUpdateTime(LocalDateTime.now());
        merchantInfo.setUpdateUser(currentUserId);

        return merchantInfoMapper.updateById(merchantInfo);
    }
}
