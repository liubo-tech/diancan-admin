package com.diancan.admin.controller;

import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.SysPermission;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.SysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("permission")
public class SysPermissionController {
    @Autowired
    private SysPermissionService permissionService;

    @PreAuthorize("hasAuthority('system:permission')")
    @RequestMapping("tree")
    public RespResult permissionTree() {
        List<SysPermission> tree = permissionService.permissionTree();
        return RespResult.success(tree);
    }

    @PreAuthorize("hasAuthority('system:permission')")
    @RequestMapping("update")
    public RespResult update(SysPermission permission, @AuthenticationPrincipal SysUser currentUser) {
        if (permission.getId() == null) {
            LocalDateTime now = LocalDateTime.now();
            permission.setCreateTime(now);
            permission.setCreateUser(currentUser.getId());
            permission.setUpdateTime(now);
            permission.setUpdateUser(currentUser.getId());

            permissionService.insertPermission(permission);
            return RespResult.success();
        } else {
            LocalDateTime now = LocalDateTime.now();
            permission.setUpdateTime(now);
            permission.setUpdateUser(currentUser.getId());

            permissionService.updatePermission(permission);
        }

        return RespResult.success();
    }

    @PreAuthorize("hasAuthority('system:permission')")
    @RequestMapping("detail")
    public RespResult detail(Integer permissionId) {
        SysPermission permission = permissionService.detail(permissionId);
        if (permission.getParentId() == 0) {
            permission.setPermissionName("根节点");
        } else {
            SysPermission parentPermission = permissionService.detail(permission.getParentId());
            permission.setParentName(parentPermission.getPermissionName());
        }

        return RespResult.success(permission);
    }

    @PreAuthorize("hasAuthority('system:permission')")
    @RequestMapping("delete")
    public RespResult delete(Integer permissionId) {
        permissionService.deleteById(permissionId);
        return RespResult.success();
    }
}
