package com.diancan.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.common.RespResult;
import com.diancan.admin.config.QiniuConfig;
import com.diancan.admin.service.entity.HomepageCarousel;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.HomepageCarouselService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("homepage/carousel")
public class HomepageCarouselController extends BaseController {
    @Autowired
    private HomepageCarouselService carouselService;

    @PreAuthorize("hasAuthority('homepage:carousel')")
    @RequestMapping("list")
    public RespResult selectByCondition(Page<HomepageCarousel> page, HomepageCarousel condition) {
        RespResult respResult = new RespResult();
        IPage<HomepageCarousel> carouselIPage = carouselService.selectByCondition(page, condition);
        respResult.setData(carouselIPage);

        return respResult;
    }

    @PreAuthorize("hasAuthority('homepage:carousel')")
    @RequestMapping("update")
    public RespResult update(HomepageCarousel carousel, @AuthenticationPrincipal SysUser sysUser) {
        LocalDateTime now = LocalDateTime.now();
        //新增
        if (carousel.getId() == null) {
            carousel.setCreateUser(sysUser.getId());
            carousel.setUpdateUser(sysUser.getId());
            carousel.setCreateTime(now);
            carousel.setUpdateTime(now);
            carouselService.addCarousel(carousel);
        } else {//修改
            carousel.setUpdateUser(sysUser.getId());
            carousel.setUpdateTime(now);
            carouselService.updateCarousel(carousel);
        }


        return RespResult.success();
    }

    @PreAuthorize("hasAuthority('homepage:carousel')")
    @RequestMapping("delete")
    public RespResult delete(Integer carouselId) {
        carouselService.deleteByCarouselId(carouselId);
        return RespResult.success();
    }
    @PreAuthorize("hasAuthority('homepage:carousel')")
    @RequestMapping("changeStatus")
    public RespResult changeStatus(Integer carouselId,@AuthenticationPrincipal SysUser sysUser) {
        carouselService.changeStatus(carouselId,sysUser.getId());
        return RespResult.success();
    }
    @PreAuthorize("hasAuthority('homepage:carousel')")
    @RequestMapping("detail")
    public RespResult detail(Integer carouselId) {
        HomepageCarousel carousel = carouselService.selectById(carouselId);
        return RespResult.success(carousel);
    }

}
