package com.diancan.admin.service.impl;

import com.diancan.admin.service.entity.SysPermission;
import com.diancan.admin.service.mapper.SysPermissionMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class SysPermissionService {
    @Autowired
    private SysPermissionMapper permissionMapper;

    public List<SysPermission> permissionTree() {
        List<SysPermission> sysPermissions = permissionMapper.selectList(null);
        if (CollectionUtils.isEmpty(sysPermissions)) return sysPermissions;

        Map<Integer, List<SysPermission>> permissionMap = sysPermissions.stream()
                .collect(groupingBy(SysPermission::getParentId));

        List<SysPermission> topLevelPermission = sysPermissions.stream()
                .filter(p->p.getParentId().equals(0))
                .collect(toList());

        findNextLevelPermission(topLevelPermission,permissionMap);

        return topLevelPermission;
    }

    private void findNextLevelPermission(List<SysPermission> topLevelPermission,
                                         Map<Integer, List<SysPermission>> permissionMap) {
        if (CollectionUtils.isEmpty(topLevelPermission)) return;
        for (SysPermission sysPermission : topLevelPermission) {
            List<SysPermission> nextLevelPermissionList = permissionMap.get(sysPermission.getId());
            if (CollectionUtils.isEmpty(nextLevelPermissionList)) continue;

            sysPermission.setChildren(nextLevelPermissionList);
            findNextLevelPermission(nextLevelPermissionList,permissionMap);
        }
    }

    public int insertPermission(SysPermission permission) {
        return permissionMapper.insert(permission);
    }

    public int updatePermission(SysPermission permission) {
        return permissionMapper.updateById(permission);
    }

    public SysPermission detail(Integer permissionId) {
        return permissionMapper.selectById(permissionId);
    }

    public int deleteById(Integer permissionId) {
        return permissionMapper.deleteById(permissionId);
    }
}
