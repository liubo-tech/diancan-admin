package com.diancan.admin.service.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-08-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserAddress implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 收货人姓名
     */
    private String receiver;

    /**
     * 收货人手机号
     */
    private String mobile;

    /**
     * 门牌号
     */
    private String houseNo;

    private String province;

    private String city;

    private String district;

    private String address;

    private String locationName;

    private BigDecimal latitude;

    private BigDecimal longitude;

    /**
     * 0：正常；1：删除
     */
    private Integer deleted;

    private String openid;

    /**
     * 是否默认
     */
    private Boolean isDefault;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}
