package com.diancan.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.SysPermission;
import com.diancan.admin.service.entity.SysRole;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.SysPermissionService;
import com.diancan.admin.service.impl.SysRoleService;
import com.diancan.admin.service.impl.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("role")
public class SysRoleController {
    @Autowired
    private SysRoleService roleService;
    @Autowired
    private SysPermissionService permissionService;
    @Autowired
    private SysUserService userService;

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("list")
    public RespResult list(Page<SysRole> page, SysRole condition) {
        RespResult respResult = new RespResult();
        respResult.setData(roleService.selectByCondition(page,condition));
        return respResult;
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("detail")
    public RespResult detail(Integer roleId) {
        RespResult respResult = new RespResult();
        respResult.setData(roleService.selectById(roleId));
        return respResult;
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("delRole")
    public RespResult deleteRole(Integer roleId) {
        RespResult respResult = new RespResult();
        respResult.setData(roleService.deleteById(roleId));
        return respResult;
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("update")
    public RespResult update(SysRole role, @AuthenticationPrincipal SysUser currentUser) {
        RespResult respResult = new RespResult();
        //新增
        if (role.getId() == null) {
            LocalDateTime now = LocalDateTime.now();
            role.setCreateTime(now);
            role.setCreateUser(currentUser.getId());
            role.setUpdateTime(now);
            role.setUpdateUser(currentUser.getId());
            roleService.insertSysRole(role);
        } else {
            SysRole sysRole = roleService.selectById(role.getId());
            if (sysRole == null) {
                respResult.setCode(1);
                respResult.setMessage("用户已被删除");
                return respResult;
            }

            role.setUpdateTime(LocalDateTime.now());
            role.setUpdateUser(currentUser.getId());

            roleService.updateRole(role);
        }

        return respResult;
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("rolePermission")
    public RespResult rolePermission(Integer roleId) {
        Set<Integer> permissionIds = roleService.getPermissionById(roleId);
        List<SysPermission> permissionTree = permissionService.permissionTree();
        Map<String,Object> map = new HashMap<>();
        map.put("permissionTree",permissionTree);
        map.put("checkedIds",permissionIds);

        return RespResult.success(map);
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("updatePermission")
    public RespResult updatePermission(@RequestParam Integer roleId,  Integer[] permissionIds) {
        roleService.updatePermission(roleId, Arrays.asList(permissionIds));
        return RespResult.success();
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("getUsers")
    public RespResult getUsers(Integer roleId, SysUser sysUser, Page<SysUser> page) {
        Set<Integer> userIds = roleService.getUsersByRoleId(roleId);
        IPage<SysUser> iPage = userService.selectByCondition(page, sysUser);

        for (SysUser record : iPage.getRecords()) {
            record.setChecked(userIds.contains(record.getId()));
        }

        return RespResult.success(iPage);
    }

    @PreAuthorize("hasAuthority('system:role')")
    @RequestMapping("updateUserRole")
    public  RespResult updateUserRole(Integer roleId,Integer userId, boolean checked) {
        if (checked) {
            roleService.addUserRole(roleId, userId);
        } else {
            roleService.deleteUserRole(roleId, userId);
        }

        return RespResult.success();
    }

}
