package com.diancan.admin.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-07-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class HomepageCarousel implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 轮播图名称
     */
    private String carousel;

    /**
     * 轮播图图片
     */
    private String carouselImg;

    /**
     * 轮播图跳转地址
     */
    private String carouselTarget;

    /**
     * 状态（0：启用；1：停用）
     */
    private Integer status;

    /**
     * 删除（0：正常；1：删除）
     */
    @JsonIgnore
    private Integer deleted;
    @JsonIgnore
    private LocalDateTime createTime;
    @JsonIgnore
    private Integer createUser;
    @JsonIgnore
    private LocalDateTime updateTime;
    @JsonIgnore
    private Integer updateUser;


}
