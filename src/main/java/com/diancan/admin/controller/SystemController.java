package com.diancan.admin.controller;

import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

import static com.diancan.admin.common.Constant.SESSION_USER_ID;
import static com.diancan.admin.common.Constant.SESSION_USER_LOGIN;

@RestController
@RequestMapping("system")
public class SystemController {

    @Autowired
    private SysUserService userService;

    @RequestMapping("loginSuccess")
    public RespResult loginSuccess() {
        RespResult respResult = new RespResult();
        return respResult;
    }

    @RequestMapping("needLogin")
    public RespResult needLogin() {
        RespResult respResult = new RespResult();
        respResult.setCode(-1);
        respResult.setMessage("请登录");
        return respResult;
    }

    @RequestMapping("logoutSuccess")
    public RespResult logoutSuccess() {
        RespResult respResult = new RespResult();
        return respResult;
    }

    @RequestMapping("loginFailure")
    public RespResult loginFailure() {
        RespResult respResult = new RespResult();
        respResult.setCode(1);
        respResult.setMessage("用户名或密码错误");
        return respResult;
    }

    @RequestMapping("userinfo")
    public RespResult userinfo() {
        RespResult respResult = new RespResult();
        SysUser sysUser = (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        respResult.setData(sysUser);
        return respResult;
    }
}
