package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.HomepageCarousel;
import com.diancan.admin.service.mapper.HomepageCarouselMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class HomepageCarouselService {
    @Autowired
    private HomepageCarouselMapper carouselMapper;

    public IPage<HomepageCarousel> selectByCondition(Page<HomepageCarousel> page, HomepageCarousel condition) {
        QueryWrapper<HomepageCarousel> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight(StringUtils.isNotBlank(condition.getCarousel()),"carousel",condition.getCarousel());
        queryWrapper.eq(condition.getStatus()!=null,"status",condition.getStatus());

        return carouselMapper.selectPage(page,queryWrapper);
    }


    public int addCarousel(HomepageCarousel carousel) {
        return carouselMapper.insert(carousel);
    }

    public int deleteByCarouselId(Integer carouselId) {
        return carouselMapper.deleteById(carouselId);
    }

    public int changeStatus(Integer carouselId, Integer currentUserId) {
        HomepageCarousel carousel = carouselMapper.selectById(carouselId);
        if (carousel != null) {
            Integer status = carousel.getStatus();

            if (status == 0) {//启用变停用
                carousel.setStatus(1);
            } else {//停用变启用
                carousel.setStatus(0);
            }
            carousel.setUpdateTime(LocalDateTime.now());
            carousel.setUpdateUser(currentUserId);
            carouselMapper.updateById(carousel);
        }

        return 0;
    }

    public HomepageCarousel selectById(Integer carouselId) {
        return carouselMapper.selectById(carouselId);
    }

    public int updateCarousel(HomepageCarousel carousel) {
        return carouselMapper.updateById(carousel);
    }
}
