package com.diancan.admin;

import com.diancan.admin.config.QiniuConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@MapperScan("com.diancan.admin.service.mapper")
@EnableConfigurationProperties(QiniuConfig.class)
public class DiancanAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiancanAdminApplication.class, args);
    }

}
