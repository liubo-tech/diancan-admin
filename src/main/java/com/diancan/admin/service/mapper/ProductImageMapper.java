package com.diancan.admin.service.mapper;

import com.diancan.admin.service.entity.ProductImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-08-07
 */
public interface ProductImageMapper extends BaseMapper<ProductImage> {

}
