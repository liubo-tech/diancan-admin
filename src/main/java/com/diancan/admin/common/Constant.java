package com.diancan.admin.common;

public interface Constant {
    String SESSION_USER_LOGIN = "SESSION_USER_LOGIN";
    String SESSION_USER_ID = "SESSION_USER_ID";

    /**
     * 商户状态：审核通过
     */
    int MERCHANT_STATUS_PASS = 2;

    /**
     * 商户状态：审核驳回
     */
    int MERCHANT_STATUS_REJECT = 0;

    /**
     * 商品状态：上架
     */
    int PRODUCT_STATUS_UP = 0;

    /**
     * 商品状态：下架
     */
    int PRODUCT_STATUS_DOWN = 1;

    /**
     * 商品状态：平台禁售
     */
    int PRODUCT_STATUS_FORBIDDEN = 2;
}
