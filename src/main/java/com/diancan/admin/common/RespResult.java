package com.diancan.admin.common;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class RespResult {
    //正常
    private int code = 0;
    //消息
    private String message ="请求成功";

    private Object data;

    public static RespResult success(){
        RespResult respResult = new RespResult();
        return respResult;
    }

    public static RespResult success(Object data){
        RespResult respResult = new RespResult();
        respResult.setData(data);
        return respResult;
    }
}
