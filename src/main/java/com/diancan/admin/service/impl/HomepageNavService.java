package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.HomepageNav;
import com.diancan.admin.service.mapper.HomepageNavMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class HomepageNavService {
    @Autowired
    private HomepageNavMapper navMapper;

    public IPage<HomepageNav> selectByCondition(Page<HomepageNav> page, HomepageNav condition) {
        QueryWrapper<HomepageNav> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight(StringUtils.isNotBlank(condition.getNavName()),"nav_name",condition.getNavName());
        queryWrapper.eq(condition.getStatus()!=null,"status",condition.getStatus());

        return navMapper.selectPage(page,queryWrapper);
    }


    public int addNav(HomepageNav nav) {
        return navMapper.insert(nav);
    }

    public int deleteByNavId(Integer navId) {
        return navMapper.deleteById(navId);
    }

    public int changeStatus(Integer navId, Integer currentUserId) {
        HomepageNav nav = navMapper.selectById(navId);
        if (nav != null) {
            Integer status = nav.getStatus();

            if (status == 0) {//启用变停用
                nav.setStatus(1);
            } else {//停用变启用
                nav.setStatus(0);
            }
            nav.setUpdateTime(LocalDateTime.now());
            nav.setUpdateUser(currentUserId);
            navMapper.updateById(nav);
        }

        return 0;
    }

    public HomepageNav selectById(Integer carouselId) {
        return navMapper.selectById(carouselId);
    }

    public int updateNav(HomepageNav carousel) {
        return navMapper.updateById(carousel);
    }
}
