package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.SysRole;
import com.diancan.admin.service.entity.SysRolePermissionRel;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.entity.SysUserRoleRel;
import com.diancan.admin.service.mapper.SysRoleMapper;
import com.diancan.admin.service.mapper.SysRolePermissionRelMapper;
import com.diancan.admin.service.mapper.SysUserRoleRelMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
public class SysRoleService {
    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysRolePermissionRelMapper rolePermissionRelMapper;
    @Autowired
    private SysUserRoleRelMapper userRoleRelMapper;

    public IPage<SysRole> selectByCondition(Page<SysRole> page, SysRole condition) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight(StringUtils.isNotBlank(condition.getRoleName()),
                "role_name",condition.getRoleName());

        return roleMapper.selectPage(page,queryWrapper);
    }

    public SysRole selectById(Integer roleId) {
        return roleMapper.selectById(roleId);
    }

    public int insertSysRole(SysRole role) {
        return roleMapper.insert(role);
    }

    public int updateRole(SysRole role) {
        return roleMapper.updateById(role);
    }

    public int deleteById(Integer roleId) {
        return roleMapper.deleteById(roleId);
    }

    public Set<Integer> getPermissionById(Integer roleId) {
        QueryWrapper<SysRolePermissionRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id",roleId);
        queryWrapper.select("permission_id");
        List<SysRolePermissionRel> sysRolePermissionRels = rolePermissionRelMapper.selectList(queryWrapper);
        return sysRolePermissionRels.stream().map(SysRolePermissionRel::getPermissionId).collect(toSet());
    }

    public int updatePermission(Integer roleId, List<Integer> permissionIds) {
        UpdateWrapper<SysRolePermissionRel> wrapper = new UpdateWrapper<>();
        wrapper.eq("role_id",roleId);
        rolePermissionRelMapper.delete(wrapper);

        if (CollectionUtils.isEmpty(permissionIds)) return 0;

        LocalDateTime now = LocalDateTime.now();
        SysUser currentUser = (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        for (Integer permissionId : permissionIds) {
            SysRolePermissionRel srr = new SysRolePermissionRel();
            srr.setCreateUser(currentUser.getId());
            srr.setCreateTime(now);
            srr.setUpdateUser(currentUser.getId());
            srr.setUpdateTime(now);
            srr.setRoleId(roleId);
            srr.setPermissionId(permissionId);
            rolePermissionRelMapper.insert(srr);
        }

        return 0;
    }

    public Set<Integer> getUsersByRoleId(Integer roleId) {
        QueryWrapper<SysUserRoleRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id",roleId);
        queryWrapper.select("user_id");
        List<SysUserRoleRel> users = userRoleRelMapper.selectList(queryWrapper);
        return users.stream().map(SysUserRoleRel::getUserId).collect(toSet());
    }

    public int addUserRole(Integer roleId, Integer userId) {
        LocalDateTime now = LocalDateTime.now();
        SysUser sysUser = (SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        SysUserRoleRel surr = new SysUserRoleRel();
        surr.setRoleId(roleId);
        surr.setUserId(userId);
        surr.setCreateUser(sysUser.getId());
        surr.setUpdateUser(sysUser.getId());
        surr.setCreateTime(now);
        surr.setUpdateTime(now);
        return userRoleRelMapper.insert(surr);
    }

    public int deleteUserRole(Integer roleId, Integer userId) {
        QueryWrapper<SysUserRoleRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        queryWrapper.eq("role_id",roleId);

        return userRoleRelMapper.delete(queryWrapper);
    }

    public Set<Integer> getRolesByUserId(Integer userId) {
        QueryWrapper<SysUserRoleRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        queryWrapper.select("role_id");

        List<SysUserRoleRel> relList = userRoleRelMapper.selectList(queryWrapper);

        return relList.stream().map(SysUserRoleRel::getRoleId).collect(toSet());
    }
}
