package com.diancan.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.common.RespResult;
import com.diancan.admin.service.entity.MerchantInfo;
import com.diancan.admin.service.entity.Product;
import com.diancan.admin.service.entity.SysUser;
import com.diancan.admin.service.impl.MerchantService;
import com.diancan.admin.service.impl.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("merchant")
public class MerchantController extends BaseController {
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private ProductService productService;

    @RequestMapping("merchantList")
    public RespResult merchantList(Page<MerchantInfo> page, MerchantInfo condition) {
        return RespResult.success(merchantService.selectByCondition(page,condition));
    }

    @RequestMapping("pass")
    public RespResult pass(@RequestParam Integer merchantId, @AuthenticationPrincipal SysUser currentUser) {
        return RespResult.success(merchantService.pass(merchantId,currentUser.getId()));
    }

    @RequestMapping("reject")
    public RespResult reject(@RequestParam Integer merchantId,
                             @RequestParam String rejectMsg,
                             @AuthenticationPrincipal SysUser currentUser) {
        return RespResult.success(merchantService.reject(merchantId,rejectMsg,currentUser.getId()));
    }

    @RequestMapping("productList")
    public RespResult productList(Page<Product> page, Product condition) {
        return RespResult.success(productService.selectByCondition(page, condition));
    }

    @RequestMapping("forbiddenProduct")
    public RespResult forbiddenProduct(Integer productId, @AuthenticationPrincipal SysUser currentUser) {
        return RespResult.success(productService.forbiddenProduct(productId, currentUser.getId()));
    }

    @RequestMapping("unforbidden")
    public RespResult unforbidden(Integer productId, @AuthenticationPrincipal SysUser currentUser) {
        return RespResult.success(productService.unforbiddenProduct(productId, currentUser.getId()));
    }
}
