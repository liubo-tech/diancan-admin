package com.diancan.admin.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.diancan.admin.service.entity.SysPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-06-22
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<SysPermission> selectByUserId(@Param("userId") Integer userId);
}
