package com.diancan.admin.service.impl;

import com.diancan.admin.service.entity.HomepageNotice;
import com.diancan.admin.service.mapper.HomepageNoticeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class HomepageNoticeService {
    @Autowired
    private HomepageNoticeMapper noticeMapper;

    public HomepageNotice loadHomepageNotice() {
        return noticeMapper.selectById(1);
    }

    public int changeStatus(Boolean status, Integer currentUserId) {
        LocalDateTime now = LocalDateTime.now();
        HomepageNotice notice = new HomepageNotice();
        notice.setId(1);
        notice.setStatus(status);
        notice.setUpdateTime(now);
        notice.setUpdateUser(currentUserId);

        return noticeMapper.updateById(notice);
    }

    public int updateNotice(HomepageNotice notice, Integer currentUserId) {
        LocalDateTime now = LocalDateTime.now();
        notice.setUpdateTime(now);
        notice.setUpdateUser(currentUserId);
        return noticeMapper.updateById(notice);
    }
}
