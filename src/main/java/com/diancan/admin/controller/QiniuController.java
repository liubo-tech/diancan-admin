package com.diancan.admin.controller;

import com.diancan.admin.common.RespResult;
import com.diancan.admin.config.QiniuConfig;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@RestController
@RequestMapping("qiniu")
public class QiniuController extends BaseController {
    @Autowired
    private QiniuConfig qiniuConfig;

    @RequestMapping("uploadImg")
    public RespResult uploadImg(MultipartFile file) throws IOException {

        //配置华东机房
        Configuration cfg = new Configuration(Region.region0());
        UploadManager uploadManager = new UploadManager(cfg);
        //获取upToken
        Auth auth = Auth.create(qiniuConfig.getAccessKey(), qiniuConfig.getSecretKey());
        String upToken = auth.uploadToken(qiniuConfig.getBucket());

        LocalDateTime now = LocalDateTime.now();
        String filename = now.toLocalDate().format(DateTimeFormatter.BASIC_ISO_DATE)+"/"+ UUID.randomUUID().toString().replace("-","");

        Response response = uploadManager.put(file.getInputStream(), filename, upToken,null,null);
        //解析上传成功的结果
        DefaultPutRet putRet = null;
        try {
            putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
        } catch (QiniuException e) {
            e.printStackTrace();
        }

        return RespResult.success(filename);
    }
}
