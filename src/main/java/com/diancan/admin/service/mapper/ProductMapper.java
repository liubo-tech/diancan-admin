package com.diancan.admin.service.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liubo
 * @since 2020-08-07
 */
public interface ProductMapper extends BaseMapper<Product> {

    IPage<Product> selectByCondition(Page<Product> page, @Param("condition") Product condition);
}
