package com.diancan.admin.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author liubo
 * @since 2020-06-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@JsonIgnoreProperties(allowSetters = true,allowGetters = false,value = "password")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysUser implements Serializable, UserDetails {

    private static final long serialVersionUID=1L;

    @TableField(exist = false)
    private boolean checked;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐
     */
    @JsonIgnore
    private String salt;

    /**
     * 状态（0：正常；1：锁定）
     */
    private Integer status;

    /**
     * 删除字段（0：未删除；1：删除）
     */
    @JsonIgnore
    private Integer deleted;

    @JsonIgnore
    private LocalDateTime createTime;

    @JsonIgnore
    private Integer createUser;

    @JsonIgnore
    private LocalDateTime updateTime;

    @JsonIgnore
    private Integer updateUser;

    @TableField(exist = false)
    private List<SysPermission> permissions;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissions;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
