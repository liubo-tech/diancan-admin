package com.diancan.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.diancan.admin.service.entity.MerchantInfo;
import com.diancan.admin.service.entity.Product;
import com.diancan.admin.service.entity.ProductImage;
import com.diancan.admin.service.entity.ProductUnit;
import com.diancan.admin.service.mapper.MerchantInfoMapper;
import com.diancan.admin.service.mapper.ProductImageMapper;
import com.diancan.admin.service.mapper.ProductMapper;
import com.diancan.admin.service.mapper.ProductUnitMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.diancan.admin.common.Constant.PRODUCT_STATUS_DOWN;
import static com.diancan.admin.common.Constant.PRODUCT_STATUS_FORBIDDEN;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class ProductService {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductImageMapper productImageMapper;
    @Autowired
    private ProductUnitMapper productUnitMapper;
    @Autowired
    private MerchantInfoMapper merchantInfoMapper;

    public IPage<Product> selectByCondition(Page<Product> page, Product condition) {
        IPage<Product> pageProduct = productMapper.selectByCondition(page,condition);
        List<Product> productList = pageProduct.getRecords();
        if (CollectionUtils.isNotEmpty(productList)) {
            List<Integer> productIds = productList.stream().map(Product::getId).collect(toList());

            QueryWrapper<ProductImage> queryWrapper = new QueryWrapper<>();
            queryWrapper.in("product_id",productIds);
            List<ProductImage> productImages = productImageMapper.selectList(queryWrapper);
            Map<Integer, List<ProductImage>> imageMap = productImages.stream().collect(groupingBy(ProductImage::getProductId));

            List<ProductUnit> productUnits = productUnitMapper.selectList(new QueryWrapper<>());
            Map<Integer, List<ProductUnit>> unitMap = productUnits.stream().collect(groupingBy(ProductUnit::getId));

            for (Product product : productList) {
                List<ProductImage> images = imageMap.get(product.getId());
                product.setImageList(images);

                List<ProductUnit> unitList = unitMap.get(product.getProductUnitId());
                if (CollectionUtils.isEmpty(unitList)) continue;

                product.setProductUnitName(unitList.get(0).getProductUnitName());
            }
        }
        return pageProduct;
    }


    public int forbiddenProduct(Integer productId, Integer currentUserId) {
        Product product = new Product();
        product.setId(productId);
        product.setProductStatus(PRODUCT_STATUS_FORBIDDEN);
        product.setUpdateTime(LocalDateTime.now());
        product.setUpdateUser(currentUserId);

        return productMapper.updateById(product);
    }

    public Object unforbiddenProduct(Integer productId, Integer currentUserId) {
        Product product = new Product();
        product.setId(productId);
        product.setProductStatus(PRODUCT_STATUS_DOWN);
        product.setUpdateTime(LocalDateTime.now());
        product.setUpdateUser(currentUserId);

        return productMapper.updateById(product);
    }
}
